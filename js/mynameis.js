const nameList = {
  "J-Hope": "./img/name/bts/J-Hope.png",
  "Jimin": "./img/name/bts/Jimin.png",
  "Jin": "./img/name/bts/Jin.png",
  "Jungkook": "./img/name/bts/Jungkook.png",
  "RM": "./img/name/bts/RM.png",
  "Suga": "./img/name/bts/Suga.png",
  "V": "./img/name/bts/V.png",
  "Minnie": "./img/name/idle/Minnie.png",
  "Miyeon": "./img/name/idle/Miyeon.png",
  "Shuhua": "./img/name/idle/Shuhua.png",
  "Soojin": "./img/name/idle/Soojin.jpg",
  "Soyeon": "./img/name/stationx0/wowthing/Soyeon.jpg",
  "Yuqi": "./img/name/idle/Yuqi.jpg",
  "Irene": "./img/name/redvelvet/powerup/Irene.jpg",
  "Joy": "./img/name/redvelvet/powerup/Joy.jpg",
  "Seulgi": "./img/name/stationx0/wowthing/Seulgi.jpg",
  "Wendy": "./img/name/redvelvet/powerup/Wendy.jpg",
  "Yeri": "./img/name/redvelvet/powerup/Yeri.jpg",
  "IU": "./img/name/solo/IU.png",
  "Jiyoon": "./img/name/bol4/Jiyoon.jpg",
  "Jiyoung": "./img/name/bol4/Jiyoung.jpg",
  "Jennie": "./img/name/blackpink/Jennie.jpg",
  "Jisoo": "./img/name/blackpink/Jisoo.jpg",
  "Lisa": "./img/name/blackpink/Lisa.jpg",
  "Rose": "./img/name/blackpink/Rose.jpg",
  "Chaeyoung": "./img/name/twice/yesoryes/Chaeyoung.png",
  "Dahyun": "./img/name/twice/yesoryes/Dahyun.png",
  "Jeongyeon": "./img/name/twice/yesoryes/Jeongyeon.png",
  "Jihyo": "./img/name/twice/yesoryes/Jihyo.png",
  "Mina": "./img/name/twice/yesoryes/Mina.png",
  "Momo": "./img/name/twice/yesoryes/Momo.png",
  "Nayeon": "./img/name/twice/yesoryes/Nayeon.png",
  "Sana": "./img/name/twice/yesoryes/Sana.png",
  "Tzuyu": "./img/name/twice/yesoryes/Tzuyu.png",
  "Eunwoo": "./img/name/pristinv/Eunwoo.png",
  "Kyulkyung": "./img/name/pristinv/Kyulkyung.png",
  "Nayoung": "./img/name/pristinv/Nayoung.png",
  "Rena": "./img/name/pristinv/Rena.png",
  "Roa": "./img/name/pristinv/Roa.png",
  "Doge": "./img/name/meme/Doge.png",
  "God": "./img/name/meme/God.png",
  "Nyan Cat": "./img/name/meme/NyanCat.gif",
  "Octocat": "./img/name/octodex/Original.png",
  "Classy Octocat": "./img/name/octodex/ClassAct.png",
  "Octobi Wan Catnobi": "./img/name/octodex/OctobiWanCatnobi.png",
  "Puppeteer": "./img/name/octodex/Puppeteer.png",
  "Scottocat": "./img/name/octodex/Scottocat.png",
  "Benevocats": "./img/name/octodex/Benevocats.png",
  "Forktocat": "./img/name/octodex/Forktocat.png",
  "Repo": "./img/name/octodex/Repo.png",
  "Cave Johnson": "./img/name/aperturescience/CaveJohnson.png",
  "GLaDOS": "./img/name/aperturescience/GLaDOS.png",
  "Daniel Shiffman": "./img/name/youtube/DanielShiffman.png",
  "Derek Banas": "./img/name/youtube/DerekBanas.png",
  "Siraj Raval": "./img/name/youtube/SirajRaval.png",
  "SinB": "./img/name/stationx0/wowthing/SinB.png",
  "Chung Ha": "./img/name/stationx0/wowthing/ChungHa.jpg",
  "Sunmi": "./img/name/sunmi/siren/Sunmi.png",
  "Hwasa": "./img/name/mamamoo/starrynight/Hwasa.png",
  "Moonbyul": "./img/name/mamamoo/starrynight/Moonbyul.jpg",
  "Solar": "./img/name/mamamoo/starrynight/Solar.jpg",
  "Wheein": "./img/name/mamamoo/starrynight/Wheein.jpg",
  "Chanhyuk": "./img/name/akmu/dinosaur/Chanhyuk.png",
  "Suhyun": "./img/name/akmu/dinosaur/Suhyun.png",
}

function addOptions() {
  const dataList = document.getElementById("nameList");
  Object.entries(nameList).forEach(
    ([key, value]) => {
      let option = document.createElement("option");
      option.value = key;
      dataList.appendChild(option);
    }
  );
}

addOptions();

function setToPicture(id, text) {
  if (Object.keys(nameList).indexOf(text) === -1) {
    return;
  }

  let showcase = document.getElementById(id);
  let imageUrl = Object.values(nameList)[Object.keys(nameList).indexOf(text)];
  let image = document.createElement("img");
  image.src = imageUrl;
  image.alt = text;

  if (showcase.childElementCount < 1) {
    showcase.appendChild(image);
  } else if (showcase.childElementCount === 1) {
    showcase.lastChild.src = imageUrl;
    showcase.lastChild.alt = text;
  }
}
